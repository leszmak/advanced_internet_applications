//Help functions
export function addContent(book, id){
    let section1 = document.createElement("div");
    section1.classList.add('bookSection');
    section1.setAttribute("id", id);

    section1 = insertBlock(section1, "");

    book.appendChild(section1);
}

export function insertBlock(section1, text){
    let x = document.createElement("INPUT");
    x.setAttribute("type", "text");
    x.setAttribute("value", text);
    section1.appendChild(x);

    return section1;
}

export function onClickSave(book, button){
    button.innerHTML = 'Edit';

    let authorBlock = book.querySelector('#Author');
    let inputAuthor = authorBlock.getElementsByTagName("input")[0];
    let author = inputAuthor.value;
    inputAuthor.remove();

    const node1 = document.createTextNode(author);
    authorBlock.appendChild(node1);

    let titleBlock = book.querySelector('#Title');
    let inputTitle = titleBlock.getElementsByTagName("input")[0];
    let title = inputTitle.value;
    inputTitle.remove();

    const node2 = document.createTextNode(title);
    titleBlock.appendChild(node2);
}

export function onClickEdit(book, button){
    button.innerHTML = 'Save';

    let authorBlock = book.querySelector('#Author');
    let pom1 = authorBlock.innerHTML;
    authorBlock.innerHTML = "";
    insertBlock(authorBlock, pom1);

    let titleBlock = book.querySelector('#Title');
    let pom2 = titleBlock.innerHTML;
    titleBlock.innerHTML = "";
    insertBlock(titleBlock, pom2);
}

export function buttonBlock(book){
    const section = document.createElement("div");
    section.setAttribute("id", "ButtonBlock");
    section.classList.add('bookSection');

    let button = document.createElement("Button");
    button.innerHTML = 'Save';
    button.onclick = function(){
        if(button.innerHTML == 'Save'){
            onClickSave(book, button);
        }
        else if(button.innerHTML == 'Edit'){
            onClickEdit(book, button);
        }
    }

    section.appendChild(button);

    let button2 = document.createElement("Button");
    button2.innerHTML = 'Remove';
    button2.onclick = function(){
        book.remove();
    }
    section.appendChild(button2);

    book.appendChild(section);
}