import {addContent, buttonBlock} from "./functions.js"
const buttonNewBook = document.getElementById("buttonNewBook");
buttonNewBook.addEventListener("click", function(){

    const book = document.createElement("div");
    book.classList.add('book');

    addContent(book, "Author");
    addContent(book, "Title");
    buttonBlock(book);

    const bookList = document.getElementById("bookList");
    const child = document.getElementById("emptyWhiteSpace");
    bookList.insertBefore(book, child);

});