// startup
// sudo systemctl start mongodb  
// systemctl status mongodb 
// npm start
const express = require('express');
const session = require("express-session");
const fs = require('fs').promises;
const app = express();
const { MongoClient, ServerApiVersion } = require("mongodb");
const url = 'mongodb://127.0.0.1:27017';

const mongo = new MongoClient(url, {
    serverApi: {
      version: ServerApiVersion.v1,
      strict: true,
      deprecationErrors: true
    }
});
const db = mongo.db("shop").collection("flowers");

let STORE_DATA;
async function initDatabase() {
  try {
    await mongo.connect();

    await db.deleteMany({});
    const data = await fs.readFile('./data.json', 'utf8');
    const myData = JSON.parse(data);
    await db.insertMany(myData.flowers);
    
    STORE_DATA = {flowers: await db.find({}).toArray()};
  } catch (err) {
    console.error(err);
  }
}

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));

app.use(session({
    secret: "secret-key",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));

app.listen(3002, async () => {
    await initDatabase();
  });

app.post("/addToCard", async (req, res) => {
    try {
        if (!req.session.card) {
            req.session.card = []; // Initialize card as an empty array
        }
        req.session.card.push(req.body.flower); // Push the flower data into the card array
        res.redirect('back');
    } catch (err) {
      console.error(err);
      res.status(500).send("Internal Server Error");
    }
});

app.post("/removeFromCard", async (req, res) => {
    try {
        let new_card = req.session.card.filter( flower => {
            return flower !== req.body.flower
        });
        req.session.card = new_card;
        res.redirect('back');

    } catch (err) {
      console.error(err);
      res.status(500).send("Internal Server Error");
    }
});

app.get('/store', function(req, res){
    res.render('store.ejs', {
        items: STORE_DATA,
    });
});

app.get('/card', function(req, res){
    let f = {flowers: []};
    let d = STORE_DATA;
    for(let i = 0; i < d.flowers.length; i++){
        if(req.session.card){
            if(req.session.card.find(element => {return element == d.flowers[i].id})){
                f.flowers.push(d.flowers[i]);
            }
        }
    }

    res.render('card.ejs', {
        items: f,
    });
});

app.post("/purchase", async (req, res) => {
  try {
    if (!req.session.card || req.session.card.length == 0) {
      res.redirect("/empty");
      return;
    }
    
    const cardIds = req.session.card.map(id => parseInt(id));
    const allItemsFound = cardIds.every(id => STORE_DATA.flowers.some(flower => flower.id === id));

    if (allItemsFound) {
      STORE_DATA.flowers = STORE_DATA.flowers.filter(flower => !cardIds.includes(flower.id));
      await db.deleteMany({ id: { $in: cardIds } });
      req.session.card = [];
      res.redirect("/bought");
    } else {
      req.session.card = [];
      res.redirect("/notbought");
    }
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

app.get("/bought", (req, res) => {
  res.render("bought");
});

app.get("/notbought", (req, res) => {
  res.render("notbought");
});

app.get("/empty", (req, res) => {
  res.render("empty");
});
