import React from 'react';

export default function Colorblock(props) {
    return (
      <div className="colorblock">
        <img src={props.url} alt={"img of color " + props.name}/>
        <div>{props.name}</div>
        <div>{props.description}</div>
        <div>
            <button className="colorBlockButton" onClick={props.onClickLike}>Like</button>
            {props.likes}
            <button className="colorBlockButton" onClick={props.onClickDislike}>Dislike</button>
        </div>
        <button onClick={props.onClickDelete} >Delete</button>
      </div>
    );
}