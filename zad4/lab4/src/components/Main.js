import Colorblock from "./ColorBlock";
import data from "../res/data.json";
import React, { useState } from 'react';

export default function Main() {
    let handleLike = (id) => {
        setState(oldValue => {
            let updatedList = oldValue.map(block => {
                if (block.key == id) {
                    let blockCopy = { ...block };
                    let propsCopy = { ...block.props };
                    propsCopy.likes += 1;
                    blockCopy.props = propsCopy;
                    return blockCopy;
                }
                return block;
            });
            return updatedList;
        });
    };

    let handleDislike = (id) => {
        setState(oldValue => {
            let updatedList = oldValue.map(block => {
                if (block.key == id) {
                    let blockCopy = { ...block };
                    let propsCopy = { ...block.props };
                    propsCopy.likes -= 1;
                    blockCopy.props = propsCopy;
                    return blockCopy;
                }
                return block;
            });
            return updatedList;
        });
    };

    let handleDelete = (id) => {
        setState(oldValue => {
            let updatedList = oldValue.filter(block => block.key != id);
            if (updatedList.length === 0) return [<div key="empty"></div>];
            
            return updatedList;
        });
    };
    
    let colorBlocks = data.colorBoxes.map(colorData => (
        <Colorblock 
            key={colorData.id}
            url={colorData.url}
            name={colorData.name}
            description={colorData.description}
            likes={colorData.likes}
            onClickLike={() => handleLike(colorData.id)}
            onClickDislike={() => handleDislike(colorData.id)}
            onClickDelete={() => handleDelete(colorData.id)}
        />
    ));

    let [state, setState] = useState(colorBlocks);

    

    let sortByName = () => {
        setState(oldValue => {
            let sortedList = oldValue.slice().sort((a, b) => {
                let nameA = a.props.name.toUpperCase(); 
                let nameB = b.props.name.toUpperCase(); 
                if (nameA < nameB) return -1;
                if (nameA > nameB) return 1;
                return 0;
            });
            return sortedList;
        });
    };

    let sortByLikes = () => {
        setState(oldValue => {
            let sortedList = oldValue.slice().sort((a, b) => b.props.likes - a.props.likes);
            return sortedList;
        });
    };

    let sortByDescription = () => {
        setState(oldValue => {
            let sortedList = oldValue.slice().sort((a, b) => {
                let descA = a.props.description.toUpperCase(); 
                let descB = b.props.description.toUpperCase(); 
                if (descA < descB) return -1;
                if (descA > descB) return 1;
                return 0;
            });
            return sortedList;
        });
    };

    const [inputState, setInputState] = useState(["url", "name", "description", 0]);
    let addNewColor = function(){
        setState(oldValue => {
            let oldValueCopy = [...oldValue,
           <Colorblock 
                key={oldValue.length + 1}
                url={inputState[0]}
                name={inputState[1]}
                description={inputState[2]}
                likes={inputState[3]}
                onClickLike={() => handleLike(oldValue.length + 1)}
                onClickDislike={() => handleDislike(oldValue.length + 1)}
                onClickDelete={() => handleDelete(oldValue.length + 1)}
            />];
            return oldValueCopy;
        });
    } 
    
    return (
        <div className="main">
            <div className="addNew">
                <input 
                    id="url"
                    type="text"
                    value={inputState[0]}
                    onChange={(e) => setInputState(oldValue => {
                            let oldValueCopy = {...oldValue}
                            oldValueCopy[0] = e.target.value;
                            return oldValueCopy;
                        }

                    )}
                />
                <input 
                    id="name"
                    type="text"
                    value={inputState[1]}
                    onChange={(e) => setInputState(oldValue => {
                            let oldValueCopy = {...oldValue}
                            oldValueCopy[1] = e.target.value;
                            return oldValueCopy;
                        }

                    )}
                />
                <input 
                    id="description"
                    type="text"
                    value={inputState[2]}
                    onChange={(e) => setInputState(oldValue => {
                            let oldValueCopy = {...oldValue}
                            oldValueCopy[2] = e.target.value;
                            return oldValueCopy;
                        }

                    )}
                />
                <input 
                    id="likes"
                    type="number"
                    value={inputState[3]}
                    onChange={(e) => setInputState(oldValue => {
                            let oldValueCopy = {...oldValue}
                            oldValueCopy[3] = e.target.value;
                            return oldValueCopy;
                        }

                    )}
                />
                <button onClick={addNewColor}>Add new color</button>
            </div>
            <div className="buttons">
                <div>Likes<button onClick={sortByLikes}>Sort by likes</button></div>
                <div>Description<button onClick={sortByDescription}>Sort by description</button></div>
                <div>Names<button onClick={sortByName}>Sort by Name</button></div>
            </div>
            {state}
        </div>
    );
}
